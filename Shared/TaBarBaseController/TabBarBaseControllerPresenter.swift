//
//  TabBarBaseControllerPresenter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class TabBarBaseControllerPresenter: TabBarBaseControllerPresenterProtocol, TabBarBaseControllerInteractorOutputProtocol {

    weak private var view: TabBarBaseControllerViewProtocol?
    var interactor: TabBarBaseControllerInteractorInputProtocol?
    private let router: TabBarBaseControllerWireframeProtocol

    init(interface: TabBarBaseControllerViewProtocol, interactor: TabBarBaseControllerInteractorInputProtocol?, router: TabBarBaseControllerWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

}
