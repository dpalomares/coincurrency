//
//  TabBarBaseControllerInteractor.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class TabBarBaseControllerInteractor: TabBarBaseControllerInteractorInputProtocol {

    weak var presenter: TabBarBaseControllerInteractorOutputProtocol?
}
