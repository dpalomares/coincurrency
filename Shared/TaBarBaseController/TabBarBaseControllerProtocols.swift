//
//  TabBarBaseControllerProtocols.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import Foundation

// MARK: Wireframe -
protocol TabBarBaseControllerWireframeProtocol: class {

}
// MARK: Presenter -
protocol TabBarBaseControllerPresenterProtocol: class {

    var interactor: TabBarBaseControllerInteractorInputProtocol? { get set }
}

// MARK: Interactor -
protocol TabBarBaseControllerInteractorOutputProtocol: class {

    /* Interactor -> Presenter */
}

protocol TabBarBaseControllerInteractorInputProtocol: class {

    var presenter: TabBarBaseControllerInteractorOutputProtocol?  { get set }

    /* Presenter -> Interactor */
}

// MARK: View -
protocol TabBarBaseControllerViewProtocol: class {

    var presenter: TabBarBaseControllerPresenterProtocol?  { get set }

    /* Presenter -> ViewController */
}
