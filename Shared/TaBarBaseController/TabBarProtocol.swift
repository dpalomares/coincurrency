//
//  TabBarProtocol.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 28/03/21.
//

import UIKit

protocol TabBarProtocol: class {
    func configuredViewController() -> UIViewController
}
