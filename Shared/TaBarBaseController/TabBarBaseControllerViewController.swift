//
//  TabBarBaseControllerViewController.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class TabBarBaseControllerViewController: UITabBarController, TabBarBaseControllerViewProtocol {

	var presenter: TabBarBaseControllerPresenterProtocol?

	override func viewDidLoad() {
        super.viewDidLoad()
    }

}
