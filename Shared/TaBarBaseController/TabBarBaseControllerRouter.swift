//
//  TabBarBaseControllerRouter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class TabBarBaseControllerRouter: TabBarBaseControllerWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = TabBarBaseControllerViewController(nibName: nil, bundle: nil)
        let interactor = TabBarBaseControllerInteractor()
        let router = TabBarBaseControllerRouter()
        let presenter = TabBarBaseControllerPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        view.viewControllers = TabBarBaseControllerRouter.controllersForTabBar()
        return view
    }
    
    static func controllersForTabBar() -> [UIViewController] {
        var viewControllers = [UIViewController]()
        let home = HomeModuleRouter()
        let controllers: [TabBarProtocol] = [home]
        
        for currentView in controllers {
            viewControllers.append(currentView.configuredViewController())
        }
        return viewControllers
    }
}
