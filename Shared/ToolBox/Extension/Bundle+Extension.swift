//
//  Bundle+Extension.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 29/03/21.
//

import Foundation

extension Bundle {
    
    enum Enviroment : String {
        case DEV
        case PROD
    }
    
    enum Plist : String {
        case rootURL = "Root_url"
        case key = "Key"
        case exchangeratesapi = "exchangeratesapi"
    }
    
    static func getKey(forKey: Plist) -> String {
        return Bundle.main.infoDictionary?[forKey.rawValue] as? String ?? ""
    }
}
