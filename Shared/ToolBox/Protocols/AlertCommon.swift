//
//  AlertCommon.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 31/03/21.
//

import UIKit

typealias Success = () -> Void

protocol AlertCommonProtocol: class {
    func showAlert(_ title: String, message: String?, complation: Success?)
}

extension AlertCommonProtocol where Self: UIViewController {
    
    func showAlert(_ title: String, message: String?, complation: Success?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: { _ in
            complation?() ?? ()
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
