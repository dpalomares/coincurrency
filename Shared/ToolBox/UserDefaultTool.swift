//
//  UserDefaultTool.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 31/03/21.
//

import Foundation

protocol UserDefaultable {
  associatedtype DefaultKey : RawRepresentable
}

extension UserDefaultable where DefaultKey.RawValue == String {
    
    static func setFavorite(_ value: String, forKey key: DefaultKey) {
      let key = key.rawValue
        guard var array = UserDefaults.getFavorite(forKey: .favorite) as? [String] else {
            UserDefaults.standard.set([value], forKey: key)
            return }
        if !array.contains(value) {
            array.append(value)
        }
      UserDefaults.standard.set(array, forKey: key)
    }
    
    static func getFavorite(forKey key: DefaultKey) -> Any? {
        return UserDefaults.standard.value(forKey: key.rawValue)
    }
    
    static func removeFavorite(_ value: String, forKey key: DefaultKey) {
        guard let array = UserDefaults.getFavorite(forKey: .favorite) as? [String] else { return }
        let favoriteUpdated = array.filter { $0 != value }
        UserDefaults.standard.set(favoriteUpdated, forKey: key.rawValue)
    }
    
}

extension UserDefaults: UserDefaultable {
  enum DefaultKey: String {
    case favorite
  }
}
