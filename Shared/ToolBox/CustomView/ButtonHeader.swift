//
//  ButtonHeader.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 31/03/21.
//

import UIKit

class ButtonHeader: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = frame.size.height / 2
        self.setTitleColor(.black, for: .normal)
        
    }
}
