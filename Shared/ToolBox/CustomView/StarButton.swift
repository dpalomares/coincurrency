//
//  StarButton.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 31/03/21.
//

import UIKit

class StarButton: UIButton {
    
    // Images
    let starDisable = UIImage(named: "starDisable")?.withRenderingMode(.alwaysOriginal)
    let starAvailable = UIImage(named: "starAvailable")?.withRenderingMode(.alwaysOriginal)
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(starAvailable, for: UIControl.State.normal)
            } else {
                self.setImage(starDisable, for: UIControl.State.normal)
             
            }
        }
    }
        
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }
        
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked.toggle()
        }
    }
}
