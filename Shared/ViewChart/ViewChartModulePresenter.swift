//
//  ViewChartModulePresenter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 30/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class ViewChartModulePresenter: ViewChartModulePresenterProtocol {
    
    weak private var view: ViewChartModuleViewProtocol?
    var interactor: ViewChartModuleInteractorProtocol?
    private let router: ViewChartModuleWireframeProtocol
    private var historicalModel: HistoricalMarketModel? {
        didSet {
            getSeries()
        }
    }
    var symbol: String
    
    
    private var arrayDays = [Date]()
    
    init(interface: ViewChartModuleViewProtocol,
         interactor: ViewChartModuleInteractorProtocol?,
         router: ViewChartModuleWireframeProtocol,
         symbol: String) {
        self.view = interface
        self.interactor = interactor
        self.router = router
        self.symbol = symbol
    }
    
    func getHistoricalMarket(_ base: String?) {
        interactor?.fetchHistoricalMarket(symbol, base: base)
    }
    
    //Private Function
    private func getSeries() {
        guard let unwrapModel = self.historicalModel?.arrayRate else { return }
        
        let sortRates = unwrapModel.sorted(by: {$0.date < $1.date})
        
        var serieData: [Double] = []
        var labels: [Double] = []
        var labelsAsString: Array<String> = []

        for (index, serie) in sortRates.enumerated() {
            serieData.append(serie.price)
            
            //convet string date to Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyy-MM-dd"
            guard let date = dateFormatter.date(from:serie.date) else { return }
            arrayDays.append(date)
            dateFormatter.dateFormat = "MM"
            let month = Int(dateFormatter.string(from: date)) ?? 0
            let monthAsString:String = String((dateFormatter.monthSymbols[month - 1]).prefix(3))
        
            if (labels.count == 0 || labelsAsString.last != monthAsString) {
                labels.append(Double(index))
                labelsAsString.append(monthAsString)
            }
        }
        view?.updateChart(serieData, labels: labels, monthStr: labelsAsString)
    }
    
    func didTapDay(_ index: Int) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMdd, YY"
        let dayString = formatter.string(from: arrayDays[index])
        return dayString
    }
    
    func updateFavorite(_ value: String, btn: Any) {
        guard let state = (btn as? StarButton)?.isChecked else { return }
        if state {
            interactor?.addFavorite(value)
        } else {
            interactor?.removeFavorite(value)
        }
    }
    
    func getFavorite() {
        interactor?.fetchFavorite()
    }
    
    //Interactor to Presenter
    func setModel(_ model: HistoricalMarketModel?) {
        if model?.error != nil {
            errorResponse(model?.error ?? "Intente más tarde")
        } else {
            self.historicalModel = model
        }
    }
    
    func errorResponse(_ error: String) {
        view?.showAlert("Error", message: error, complation: { [weak self] in
            self?.router.dismissController()
        })
    }
    
    func setFavorite(_ array: [String]) {
        let state = array.contains(symbol)
        view?.updateFavorite(state)
    }
    
}
