//
//  ViewChartModuleProtocols.swift
//  CoinCurrency
//
//  Created Diego Palomares on 30/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import Foundation

// MARK: Wireframe -
protocol ViewChartModuleWireframeProtocol: class {
    func dismissController()
}
// MARK: Presenter -
protocol ViewChartModulePresenterProtocol: class {
    var symbol: String { get set }
    func getHistoricalMarket(_ base: String?)
    func setModel(_ model: HistoricalMarketModel?)
    func didTapDay(_ index: Int) -> String
    func errorResponse(_ error: String)
    func updateFavorite(_ value: String, btn: Any)
    func getFavorite()
    func setFavorite(_ array: [String])
}

// MARK: Interactor -
protocol ViewChartModuleInteractorProtocol: class {

  var presenter: ViewChartModulePresenterProtocol?  { get set }
    func fetchHistoricalMarket(_ symbol: String, base: String?)
    func removeFavorite(_ value: String)
    func addFavorite(_ value: String)
    func fetchFavorite()
}

// MARK: View -
protocol ViewChartModuleViewProtocol: AlertCommonProtocol {

  var presenter: ViewChartModulePresenterProtocol?  { get set }
    func updateChart(_ serieData: [Double], labels: [Double], monthStr: Array<String>)
    func updateFavorite(_ sate: Bool)
}
