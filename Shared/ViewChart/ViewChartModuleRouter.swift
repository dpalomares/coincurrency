//
//  ViewChartModuleRouter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 30/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class ViewChartModuleRouter: ViewChartModuleWireframeProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(_ symbol: String) -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = ViewChartModuleViewController(nibName: nil, bundle: nil)
        let interactor = ViewChartModuleInteractor()
        let router = ViewChartModuleRouter()
        let presenter = ViewChartModulePresenter(interface: view, interactor: interactor, router: router, symbol: symbol)
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }
    
    func dismissController() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
