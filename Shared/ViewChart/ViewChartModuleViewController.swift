//
//  ViewChartModuleViewController.swift
//  CoinCurrency
//
//  Created Diego Palomares on 30/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit
import SwiftChart
import FlagKit
class ViewChartModuleViewController: UIViewController, ViewChartModuleViewProtocol {
    
    //MARK: - Variables
    var presenter: ViewChartModulePresenterProtocol?
    @IBOutlet weak var chart: Chart!
    @IBOutlet weak var constraintLeading: NSLayoutConstraint!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var flagImg: UIImageView!
    @IBOutlet weak var lblNameCoin: UILabel!
    @IBOutlet weak var btnFavortie: StarButton!
    
    var array: Array<Dictionary<String, Any>>?
    fileprivate var labelLeadingMarginInitialConstant: CGFloat!
    
    //MARK: - Life Cicle
	override func viewDidLoad() {
        super.viewDidLoad()
        labelLeadingMarginInitialConstant = constraintLeading.constant
        presenter?.getHistoricalMarket("EUR")
        presenter?.getFavorite()
        print(UserDefaults.getFavorite(forKey: .favorite) as? [String])
    }
    
    //MARK: - Functions
    func updateChart(_ serieData: [Double], labels: [Double], monthStr: Array<String>) {
        let series = ChartSeries(serieData)
        series.area = true
        chart.delegate = self
        // Configure chart layout
        chart.lineWidth = 0.5
        chart.labelFont = UIFont.systemFont(ofSize: 12)
        chart.xLabels = labels
        chart.xLabelsFormatter = { (labelIndex: Int, labelValue: Double) -> String in
            return monthStr[labelIndex]
        }
        chart.xLabelsTextAlignment = .center
        chart.yLabelsOnRightSide = true
        // Add some padding above the x-axis
        chart.minY = (serieData.min() ?? 0) - 5
        
        chart.add(series)
        updateHeader() 
    }
    
    func updateHeader() {
        lblNameCoin.text = presenter?.symbol
        flagImg.image = Flag(countryCode: String(presenter?.symbol.prefix(2) ?? ""))?.originalImage
    }
    
    @IBAction func addFavorite(_ sender: Any) {
        guard let name = lblNameCoin.text else { return }
        presenter?.updateFavorite(name, btn: sender)
    }
    
    func updateFavorite(_ sate: Bool) {
        btnFavortie.isChecked = sate
    }
}

extension ViewChartModuleViewController: ChartDelegate {
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        if let value = chart.valueForSeries(0, atIndex: indexes[0]) {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.minimumFractionDigits = 2
            numberFormatter.maximumFractionDigits = 2
            guard let day = presenter?.didTapDay(indexes[0] ?? 0) else { return }
            lblRate.text = (numberFormatter.string(from: NSNumber(value: value)) ?? "") + "\n" + day
            
            // Align the label to the touch left position, centered
            var constant = labelLeadingMarginInitialConstant + left - (lblRate.frame.width / 2)
            
            // Avoid placing the label on the left of the chart
            if constant < labelLeadingMarginInitialConstant {
                constant = labelLeadingMarginInitialConstant
            }
            
            // Avoid placing the label on the right of the chart
            let rightMargin = chart.frame.width - lblRate.frame.width
            if constant > rightMargin {
                constant = rightMargin
            }
            
            constraintLeading.constant = constant
            
        }
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        lblRate.text = ""
        constraintLeading.constant = labelLeadingMarginInitialConstant
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
}

