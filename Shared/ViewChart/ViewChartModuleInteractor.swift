//
//  ViewChartModuleInteractor.swift
//  CoinCurrency
//
//  Created Diego Palomares on 30/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class ViewChartModuleInteractor: ViewChartModuleInteractorProtocol {

    weak var presenter: ViewChartModulePresenterProtocol?
    
    func fetchHistoricalMarket(_ symbol: String, base: String?) {
        ManagerNetworking().request(CoinCurrencyEndPoint.historicalMarket(symbol: symbol, base: base ?? "EUR", start_at: "2021-01-01", end_at: "2021-03-31"), response: HistoricalMarketModel.self) { (response) in
            switch response {
            case .success(let model):
                self.presenter?.setModel(model)
            case .failure(let error):
                self.presenter?.errorResponse(error.localizedDescription)
            }
        }
    }
    
    func removeFavorite(_ value: String) {
        UserDefaults.setFavorite(value, forKey: .favorite)
    }
    
    func addFavorite(_ value: String) {
        UserDefaults.removeFavorite(value, forKey: .favorite)
    }
    
    func fetchFavorite() {
        guard let arrayCurrency = UserDefaults.getFavorite(forKey: .favorite) as? [String] else { return }
        presenter?.setFavorite(arrayCurrency)
    }
}
