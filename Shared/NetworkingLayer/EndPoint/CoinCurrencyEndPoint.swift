//
//  CoinCurrency.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 29/03/21.
//

import Foundation
import Alamofire

public enum CoinCurrencyEndPoint {
    case getCurrentPrice(base: String)
    case historicalMarket(symbol: String, base: String, start_at: String, end_at: String)
}

extension CoinCurrencyEndPoint: EndPointType {
    public var baseURL: String {
        return Bundle.getKey(forKey: .exchangeratesapi)
    }
    
    public var path: String {
        switch self {
        case .getCurrentPrice(let base):
            return "/latest?base=\(base)"
        case .historicalMarket(symbol: let symbol, base: let base, let start_at, let end_at):
            return "/history?start_at=\(start_at)&end_at=\(end_at)&symbols=\(symbol)&base=\(base)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        return .get
    }
    
    public var headers: HTTPHeaders? {
        return nil
    }
    
    
}
