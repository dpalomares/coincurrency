//
//  EndPointType.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 29/03/21.
//

import Foundation
import Alamofire

public protocol EndPointType {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
}
