//
//  ManagerNetworking.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 29/03/21.
//

import Foundation
import Alamofire

enum NetworkResponse<T> {
  case success(T?)
  case failure(Error)
}

struct ManagerNetworking {
    
    func request<T: Decodable>(_ endPoint: EndPointType, response: T.Type, completion: @escaping (NetworkResponse<T>) -> Void) {
        AF.request(endPoint.baseURL + endPoint.path,
                   method: endPoint.httpMethod).response { (response) in
                    switch response.result {
                    case .success(let value):
                        do {
                            guard let data = value else { return }
                            NetworkLogger.log(response: data)
                            let apiResponse = try JSONDecoder().decode(T.self, from: data)
                            completion(.success(apiResponse))
                        } catch {
                            completion(.failure(error))
                         
                        }
                    case .failure(let error):
                        completion(.failure(error))
                    }
        }
    }
}
