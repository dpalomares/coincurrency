//
//  Logger.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 29/03/21.
//

import Foundation

class NetworkLogger {

    static func log(response: Data) {
        print("\n - - - - - - - - - - RESPONSE - - - - - - - - - - \n")
        defer { print("\n - - - - - - - - - -  END - - - - - - - - - - \n") }
        do {
            let jsonData = try JSONSerialization.jsonObject(with: response, options: JSONSerialization.ReadingOptions.allowFragments)
            print(jsonData)
        } catch {
            print(error)
        }
    }
}
