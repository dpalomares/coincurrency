//
//  AppDelegate.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 28/03/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    window = UIWindow(frame: UIScreen.main.bounds)
    let rootWindow = TabBarBaseControllerRouter.createModule()
    window!.rootViewController = rootWindow
    window!.makeKeyAndVisible()
    
    return true
  }
}
