//
//  HomeModuleRouter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class HomeModuleRouter: TabBarProtocol, HomeModuleWireframeProtocol {
   
    //MARK: - VARIABLES
    weak var viewController: UIViewController?
    
    //MARK: - FUNCTIONS
    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = HomeModuleViewController(nibName: nil, bundle: nil)
        let interactor = HomeModuleInteractor()
        let router = HomeModuleRouter()
        let presenter = HomeModulePresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

    func configuredViewController() -> UIViewController {
        let currentView = HomeModuleRouter.createModule()
        let imgHome = UIImage(named: "Home")
        currentView.tabBarItem = UITabBarItem(title: "", image: imgHome, selectedImage: imgHome)
        return currentView
    }
    
    func showChart(_ symbol: String) {
        let chartView = ViewChartModuleRouter.createModule(symbol)
        viewController?.present(chartView, animated: true, completion: nil)
    }
    
}
