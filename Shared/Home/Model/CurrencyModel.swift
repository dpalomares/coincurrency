//
//  CurrencyModel.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 28/03/21.
//

import Foundation

//Response
struct CurrencyModel: Codable {
    
    let success: Bool?
    let base: String?
    let date: String?
    var rates: [String: Double]?
    var arrayRate = [CleanRates]()
    
    enum CodingKeys: String, CodingKey {
        case success
        case base
        case date
        case rates
    }
    
    // MARK: - INITIALIZERS
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try? container.decode(Bool.self, forKey: .success)
        self.base = try? container.decode(String.self, forKey: .base)
        self.date = try? container.decode(String.self, forKey: .date)
        self.rates = try? container.decode([String: Double].self, forKey: .rates)
        for (key, value) in self.rates ?? [String: Double]() {
            let currentIndexKey = CleanRates(key: key, price: value)
            arrayRate.append(currentIndexKey)
        }
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.success, forKey: .success)
        try? container.encode(self.base, forKey: .base)
        try? container.encode(self.date, forKey: .date)
    }
}

struct CleanRates: Codable {
    let key: String
    let price: Double
}
