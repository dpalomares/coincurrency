//
//  HistoricalMarketModel.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 30/03/21.
//

import Foundation

//Response
struct HistoricalMarketModel: Codable {
    
    var base: String?
    var rates: [String: [String: Double]]?
    var arrayRate = [CleanRatesHistorical]()
    var error: String?
    
    enum CodingKeys: String, CodingKey {
        case base
        case rates
        case error
    }
    
    // MARK: - INITIALIZERS
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.base = try? container.decode(String.self, forKey: .base)
        self.rates = try? container.decode([String: [String: Double]].self, forKey: .rates)
        self.error = try? container.decode(String.self, forKey: .error)
        for (key, value) in self.rates ?? [String: [String: Double]]() {
            let intIndex = 0
            let index = value.index(value.startIndex, offsetBy: intIndex)
            let currentIndexKey = CleanRatesHistorical(date: key, price: value[value.keys[index]] ?? 0.0)
            arrayRate.append(currentIndexKey)
        }
    }
}

struct CleanRatesHistorical: Codable {
    let date: String
    let price: Double
}
