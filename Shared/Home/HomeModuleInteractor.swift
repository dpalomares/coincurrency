//
//  HomeModuleInteractor.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class HomeModuleInteractor: HomeModuleInteractorInputProtocol {

    weak var presenter: HomeModuleInteractorOutputProtocol?
    
    func fetchCurrencyList(_ base: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            ManagerNetworking().request(CoinCurrencyEndPoint.getCurrentPrice(base: base), response: CurrencyModel.self) { (response) in
                switch response {
                case .success(let model):
                    self.presenter?.responseCurrencyList(model)
                case .failure(let error):
                    self.presenter?.responseError(error.localizedDescription)
                }
            }
        }
    }
    
    func fetchFavorite() {
        guard let arrayCurrency = UserDefaults.getFavorite(forKey: .favorite) as? [String] else { return }
        presenter?.setFavorite(arrayCurrency)
    }
}
