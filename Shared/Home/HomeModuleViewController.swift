//
//  HomeModuleViewController.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit
import SkeletonView

class HomeModuleViewController: UIViewController, HomeModuleViewProtocol {
    
    //MARK: - Variables
	var presenter: HomeModulePresenterProtocol?
    
    @IBOutlet weak var btnBase: ButtonHeader!
    @IBOutlet weak var tableViewCurrency: UITableView!
    @IBOutlet weak var stackHeader: UIStackView!
    
    //MARK: - Life cicle
	override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.viewDidLoad()
        presenter?.getCurrencyList("USD")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addSkeletonView()
    }
    
    //MARK: - ViewProtocol
    func setUpUI() {
        tableViewCurrency.dataSource = self
        tableViewCurrency.delegate = self
        tableViewCurrency.register(UINib(nibName: "CurrencyCell", bundle: nil), forCellReuseIdentifier: CurrencyCell.identifier)
        tableViewCurrency.estimatedRowHeight = 80
        tableViewCurrency.rowHeight = 80
    }
    
    func updateTable() {
        self.view.subviews.forEach { (view) in
            view.stopSkeletonAnimation()
            view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
        }
        tableViewCurrency.reloadData()
        btnBase.layoutIfNeeded()
    }
    
    func updateLabelBase() {
        btnBase.setTitle(btnBase.titleLabel?.text == "EUR" ? "USD" : "EUR", for: .normal)
    }

    @IBAction func didTapOption(_ sender: Any) {
        guard let btnSender = (sender as? UIButton) else { return }
        presenter?.didTapOption(btnSender)
    }
    
    func addSkeletonView() {
        tableViewCurrency.isSkeletonable = true
        tableViewCurrency.showAnimatedGradientSkeleton()
        stackHeader.showGradientSkeleton()
    }
    
    func showAlerOption() {
        let alert = UIAlertController(title: "Sort By", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Price ⬆⬇", style: .default , handler:{ (UIAlertAction)in
            self.presenter?.sortedArrayByPrice()
        }))
        
        alert.addAction(UIAlertAction(title: "Alphabetically", style: .default , handler:{ (UIAlertAction)in
            self.presenter?.sortedArrayAlphabetically()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension HomeModuleViewController: SkeletonTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.rowOfCell() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.identifier, for: indexPath) as? CurrencyCell
        cell?.model = presenter?.getData(indexPath.row)
        return cell ?? UITableViewCell()
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return CurrencyCell.identifier
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.willShowChart(presenter?.getData(indexPath.row)?.key ?? "")
    }
    
    
}

// MARK: - UITableViewDelegate
extension HomeModuleViewController: UITableViewDelegate {
   
}
