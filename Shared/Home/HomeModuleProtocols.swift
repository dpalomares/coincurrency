//
//  HomeModuleProtocols.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

// MARK: Wireframe -
protocol HomeModuleWireframeProtocol: class {
    func showChart(_ symbol: String)
}
// MARK: Presenter -
protocol HomeModulePresenterProtocol: class {

    var interactor: HomeModuleInteractorInputProtocol? { get set }
    var model: CurrencyModel? { get set }
    func viewDidLoad()
    func getCurrencyList(_ base: String)
    func rowOfCell() -> Int?
    func getData(_ row: Int) -> CleanRates?
    func willShowChart(_ symbol: String)
    func didTapOption(_ index: UIButton)
    func sortedArrayByPrice()
    func sortedArrayAlphabetically()
    
    
}

// MARK: Interactor -
protocol HomeModuleInteractorOutputProtocol: class {

    /* Interactor -> Presenter */
    func responseCurrencyList(_ model: CurrencyModel?)
    func responseError(_ message: String?)
    func setFavorite(_ array: [String])
}

protocol HomeModuleInteractorInputProtocol: class {

    var presenter: HomeModuleInteractorOutputProtocol?  { get set }

    /* Presenter -> Interactor */
    func fetchCurrencyList(_ base: String)
    func fetchFavorite()
}

// MARK: View -
protocol HomeModuleViewProtocol: AlertCommonProtocol {

    var presenter: HomeModulePresenterProtocol?  { get set }

    /* Presenter -> ViewController */
    func setUpUI()
    func updateTable()
    func addSkeletonView()
    func showAlerOption()
    func updateLabelBase()
}
