//
//  CurrencyCell.swift
//  CoinCurrency (iOS)
//
//  Created by Diego Palomares on 28/03/21.
//

import UIKit
import SkeletonView
import FlagKit
class CurrencyCell: UITableViewCell {
    
    //MARK: - Variables
    static let identifier = "cell"
    var model: CleanRates? {
        didSet {
            guard let model = model else { return }
            self.amountCurrency.text = String(format: "%.2f", model.price)
            self.nameCurrency.text = model.key
            let flag = Flag(countryCode: String(model.key.prefix(2)))
            self.imgFlag.image = flag?.originalImage
        }
    }
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var nameCurrency: UILabel!
    @IBOutlet weak var amountCurrency: UILabel!
    
    func setUp() {
        self.amountCurrency.text = "$234234"
        self.nameCurrency.text = "EUR"
    }
    
    
    
}
