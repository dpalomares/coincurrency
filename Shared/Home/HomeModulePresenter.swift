//
//  HomeModulePresenter.swift
//  CoinCurrency
//
//  Created Diego Palomares on 28/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class HomeModulePresenter: HomeModulePresenterProtocol {

    weak private var view: HomeModuleViewProtocol?
    var interactor: HomeModuleInteractorInputProtocol?
    private let router: HomeModuleWireframeProtocol
    internal var model: CurrencyModel? {
        didSet {
            view?.updateTable()
        }
    }
    var sort: Bool = false
    init(interface: HomeModuleViewProtocol, interactor: HomeModuleInteractorInputProtocol?, router: HomeModuleWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.setUpUI()
    }
    
    func getCurrencyList(_ base: String) {
        interactor?.fetchCurrencyList(base)
    }
    
    func rowOfCell() -> Int? {
        return model?.arrayRate.count
    }
    
    func getData(_ row: Int) -> CleanRates? {
        return model?.arrayRate.isEmpty ?? false ? nil : model?.arrayRate[row]
    }
    
    func willShowChart(_ symbol: String) {
        router.showChart(symbol)
    }
    
    func didTapOption(_ index: UIButton) {
        switch index.tag {
        case 0:
            interactor?.fetchFavorite()
        case 1:
            view?.addSkeletonView()
            interactor?.fetchCurrencyList(index.titleLabel?.text ?? "USD")
            view?.updateLabelBase()
        case 2:
            view?.showAlerOption()
        default:
            break
        }
    }
    
    func sortedArrayAlphabetically() {
        if !sort {
            model?.arrayRate.sort(by: {$0.key < $1.key})
        } else {
            model?.arrayRate.sort(by: {$0.key > $1.key})
        }
        sort.toggle()
    }
    
    func sortedArrayByPrice() {
        if !sort {
            model?.arrayRate.sort(by: {$0.price > $1.price})
        } else {
            model?.arrayRate.sort(by: {$0.price < $1.price})
        }
        sort.toggle()
    }

}

extension HomeModulePresenter: HomeModuleInteractorOutputProtocol {
    func responseCurrencyList(_ model: CurrencyModel?) {
        self.model = model
        sortedArrayAlphabetically()
    }
    
    func responseError(_ message: String?) {
        view?.showAlert("Error", message: message, complation: nil)
    }
    
    func setFavorite(_ array: [String]) {
        if sort {
            var filterModel = [CleanRates]()
                guard let filterNames = self.model?.arrayRate.filter({ array.contains($0.key) }) else { return }
                filterModel = filterNames
            self.model?.arrayRate = filterModel
        } else {
            view?.addSkeletonView()
            interactor?.fetchCurrencyList("USD")
        }
        sort.toggle()
     
    }
}
